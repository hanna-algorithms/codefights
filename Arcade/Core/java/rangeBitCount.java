int rangeBitCount(int a, int b) {
    int c=0;
    for(int i=a; i<=b; i++)
        c += Integer.bitCount(i);
    return c;
}

// bit count
// while (n > 0) {
//     c += n & 1;
//     n >>= 1;
// }

