int circleOfNumbers(int n, int f) {
	// return (f + n/2) % n
    return f >= n/2 ? f - n/2 : f + n/2;
}
