boolean tennisSet(int s1, int s2) {
    if((s1 == 6 && s2 < 5) || (s2 == 6 && s1 < 5))
        return true;
    else if((s1 >= 5 && s2 >= 5) && (s1 == 7 || s2 == 7) && s1 != s2)
        return true;
    
    return false;
}
