int knapsackLight(int value1, int weight1, int value2, int weight2, int maxW) {
	return knapsack01(new int[]{weight1, weight2}, new int[]{value1, value2}, maxW, 2);
}


int knapsack01(int w[], int v[], int W, int n) {
	if(W  == 0 || n == 0) return 0;

	return w[n-1] > W ? knapsack01(w, v, W, n-1) // next item->weight overflows => skip it
		: Math.max(v[n-1] + knapsack01(w, v, W-w[n-1], n-1), knapsack01(w, v, W, n-1)); // choose or skip next item depending on its weight (could be minus)
}