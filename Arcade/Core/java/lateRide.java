int phoneCall(int min1, int min2, int min11, int s) {
    if(s < min1) return 0;
    
    if(s < min1+9*min2)
        return 1 + (s - min1) / min2;
        
    return 10 + (s - min1 - 9*min2) / min11;
}


int phoneCall1(int min1, int min2_10, int min11, int s) {
    int m = 0;

    m = s >= min1 ? 1 : 0;
    s -= s >= min1 ? min1 : 0;

    if(s == 0) return m;
    m += s >= min2_10*9 ? 9 : (s/min2_10);
    s -= min2_10 * (s >= min2_10*9 ? 9 : s/min2_10););
    
    if(s > 0) m += s/min11;
    
    return m;
}
