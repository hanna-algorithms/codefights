int digitDegree(int n) {    
    if(n < 10) return 0; // 1 digit number
    
    int s=0;
    while(n > 0) {
        s += n%10;      // sum of digits
        n = n/10;
    }
    
    return 1 + digitDegree(s);
}



// a version with helper function
int digitDegree(int n) {
    return digitDegreeHelper(n, 0);
}

int digitDegreeHelper(int n, int deg) {
    if(n < 10) return deg; // 1 digit number
    
    int s=0;
    while(n > 0) {
        s += n%10;      // sum of digits
        n = n/10;
    }
    
    return digitDegreeHelper(s, ++deg);
}
