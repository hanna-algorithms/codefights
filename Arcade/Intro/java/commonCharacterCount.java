int commonCharacterCount(String s1, String s2) {
    int[] a = new int[26];
    int[] b = new int[26];
    
    for(char c: s1.toCharArray())
        a[c - 'a']++;
    for(char c: s2.toCharArray())
        b[c - 'a']++;
    
    int c=0;    
    for(int i=0; i < 26; i++)
        c += Math.min(a[i], b[i]);
    
    return c;
}
