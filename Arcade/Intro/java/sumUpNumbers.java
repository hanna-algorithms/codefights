int sumUpNumbers(String s) {
    return Pattern.compile("(\\D+)").splitAsStream(s).filter(item->!item.isEmpty()).mapToInt(Integer::new).sum();
    //return Arrays.stream(s.split("\\D+")).filter(item->!item.isEmpty()).mapToInt(Integer::new).sum();
}

int sumUpNumbers1(String s) {
    Matcher m = Pattern.compile("\\d+").matcher(s);
    int sum=0;

    while(m.find())
        sum += Integer.parseInt(m.group(0));

    return sum;
}