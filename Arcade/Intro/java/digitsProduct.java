int digitsProduct(int product) {
    List<Integer> digits = new ArrayList();
    int answer = 0;

    if(product == 0) return 10;
    if (product == 1) return 1;

    for(int d=9; d > 1; d--)
        while(product % d == 0) {
            product /= d;
            digits.add(d);
        }

    if (product > 1) return -1;

    Collections.sort(digits);
    for(Integer d : digits)
        answer = 10 * answer + d;

    return answer;
}

int digitsProduct1(int product) {
    int min = Integer.MAX_VALUE;

    Set<Integer> set = new HashSet();
    set = digits(product, set);
    //set.stream().forEach(System.out::println);

    return product;
}

List<Integer> digits(int n, Set<Integer> set) {
    if(1 < n && n <= 9) {
        list.add(n);
        return list;
    }

    for(int i=2; i < n / 2; i++) {
        if(n % i == 0) {
            if(1 < i && i <= 9) 
                list.add(i);
            else
                digits(i, list);

            int t = (int) n / i;
            if(1 < t && t <= 9) 
                list.add(t);
            else
                digits(t, list);
        }
    }
    return list;
}
