boolean bishopAndPawn(String b, String p) {
    return Math.abs(b.charAt(0) - p.charAt(0)) == Math.abs(b.charAt(1) - p.charAt(1));
}
