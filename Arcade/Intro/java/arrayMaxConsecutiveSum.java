int arrayMaxConsecutiveSum(int[] a) {
	int max1 = Integer.MIN_VALUE, max2 = 0;

	for (int i = 0; i < a.length; i++) {
		max2 += a[i];
		if (max1 < max2) max1 = max2;
		if (max2 < 0) max2 = 0;
	}

	return max2;
}


int arrayMaxConsecutiveSum1(int[] a, int k) {
    int max = Integer.MIN_VALUE;

    for(int i=0; i < a.length - k + 1; i++)
        max = Math.max(max, IntStream.range(i, i+k).map(j->a[j]).sum());

    return max;
}