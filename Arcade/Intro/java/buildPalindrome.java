String buildPalindrome(String s) {
    int n = s.length();
	
    // assume s as palindrome 
    int i = n/2-1;
    int j = (n/2) + n%2;

    while(i >= 0 && j < s.length()) {
        // compare middle characters
        if(s.charAt(i) == s.charAt(j)) {
            i--; j++;
        } else {
            n++; // assume char is appended, so length is increased
            i = n/2-1;
            j = (n/2) + n%2;
        }
    }

    if(n > s.length()) s += (new StringBuilder(s.substring(0, i+1))).reverse();
    return s;
}
