int[] extractEachKth(int[] a, int k) {
    return IntStream.range(0, a.length).filter(i -> ((i+1)%k != 0)).map(i -> a[i]).toArray();
}