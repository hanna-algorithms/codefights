int adjacentElementsProduct(int[] a) {
    return IntStream.range(0, a.length-1).map(i -> a[i] * a[i+1]).max().getAsInt();
}

int adjacentElementsProduct1(int[] a) {
    int max = a[0] * a[1];
    for(int i=1; i < a.length - 1; i++)
        max = Math.max(max, a[i] * a[i+1]);

    return max;
}
