boolean sudoku(int[][] grid) {
    for (int i = 0; i < 9; i++) {
        int h = 0, v = 0, b = 0;
        for (int j = 0; j < 9; j++) {
            h ^= 1 << grid[i][j];
            v ^= 1 << grid[j][i];
            b ^= 1 << grid[i - i % 3 + j / 3][i % 3 * 3 + j % 3];
        }
        
        if (h != 1022 || v != 1022 || b != 1022)
            return false;
    }
    
    return true;
}

boolean sudoku1(int[][] grid) {
    Set<Integer> s;
    
    // check horizontally
	for(int r=0; r < 9; r++) {
        s = new HashSet();
        
        for(int c=0; c < 9; c++) {
			if(s.contains(grid[r][c])) return false;
            s.add(grid[r][c]);
		}

        if(s.size() != 9) return false;
    }

	// check vertically
	for(int c=0; c < 9; c++) {
        s = new HashSet();
        
        for(int r=0; r < 9; r++) {
			if(s.contains(grid[r][c])) return false;
			s.add(grid[r][c]);
		}

        if(s.size() != 9) return false;
    }
    
    // check each 3x3
    for(int r=0; r < 9; r+=3) {
        for(int c=0; c < 9; c+=3) {
            s = new HashSet();
            
            for(int i=r; i < r+3; i++)
                for(int j=c; j < c+3; j++) {
					if(s.contains(grid[i][j])) return false;
					s.add(grid[i][j]);
				}

            if(s.size() != 9) return false;
        }
    }

    return true;
}



boolean sudoku2(int[][] grid) {
    return sudoku2(grid, 0, 0);
}


boolean sudoku2(int[][] grid, int r, int c) {
    Set<Integer> s;

    for(int i=r; i < r+3; i++) {
        for(int j=c; j < c+3; j++) {
			if(s.contains(grid[i][j])) return false;
            s.add(grid[i][j]);
        }
	}

    if(s.size() != 9) return false;
	
	if(r >= 9) return true;
	else if(c == 9 - 3) {r += 3; c = 0;}
    else c+= 3;

    return sudoku(grid, r, c);
}

