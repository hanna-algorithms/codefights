int[][] spiralNumbers(int n) {
    int[][] array = new int[n][n];
    
    int left = 0;
    int right = n - 1;
    int top = 0;
    int down = n - 1;
    
    for (int counter = 1; counter <= n * n; )
    {
        for (int x = left; x <= right; x++)
            array[top][x] = counter++;
        top++;
        
        for (int y = top; y <= down; y++)
            array[y][right] = counter++;
        right--;
        
        for (int x = right; x >= left; x--)
            array[down][x] = counter++;
        down--;
        
        for (int y = down; y >= top; y--)
            array[y][left] = counter++;
        left++;
    }
    
    return array;
}


int[][] spiralNumbers1(int n) {
    int[][] m = new int[n][];
    for (int i=0; i < n; i++)
        m[i] = new int[n];
    
    int z=1, i=0;
    while (n > 0){
        for (int j=0; j < n; j++) m[i][j+i] = z++;
        for (int k=i+1; k < n+i; k++) m[k][n-1+i] = z++;
        for (int o=n-2+i; o >= i; o--) m[n-1+i][o] = z++;
        for (int p=n-2+i; p > i; p--) m[p][i] = z++;
        i++;
        n -= 2;
    }
    return m;  
}

