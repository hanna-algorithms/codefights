char firstDigit(String s) {
    for(char c : s.toCharArray()) if (Character.isDigit(c)) return c;
    return '0';
}
