String[] allLongestStrings(String[] a) {
    int m = Arrays.stream(a).max(Comparator.comparingInt(String::length)).get().length();
    
    return Arrays.stream(a).filter(s -> (s.length() == m)).toArray(size -> new String[size]);
}


String[] allLongestStrings1(String[] a) {
    int m = Arrays.stream(a).max(Comparator.comparingInt(String::length)).get().length();
	
    List<String> l = new ArrayList();
    for (int i = 0; i < a.length; i++)
        if (m == a[i].length()) l.add(a[i]);

    return l.toArray(new String[l.size()]);
}
