int growingPlant(int us, int ds, int dh) {
    int d=1; int h=0;
    while(h <= dh) {
        if((h += us) >= dh) return d;
        h -= ds;
        d++;
    }
    return d;
}

// The height after x days (up - down) * (x - 1) + up
// We want dh <= (u - d) * (x - 1) + u
// (dh - u) / (u - d) + 1 <= x
// return Math.max(0, (int) Math.ceil((double) (desiredHeight - upSpeed) / (upSpeed - downSpeed))) + 1;

