int makeArrayConsecutive2(int[] s) {
     int max = s[0];
     int min = s[0];
     for(int i=1; i < s.length; i++) {
        max = Math.max(max, s[i]);
        min = Math.min(min, s[i]);
     }
     return max - min - s.length + 1;
}

int makeArrayConsecutive21(int[] s) {
     int max = IntStream.range(0, s.length).map(i->s[i]).max().getAsInt();
     int min = IntStream.range(0, s.length).map(i->s[i]).min().getAsInt();
     return max - min - s.length + 1;
}