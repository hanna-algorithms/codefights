String lineEncoding(String s) {
    String r="";
    s += "-";
	int c=1;
	
    for(int i=0; i < s.length()-1; i++)
        if(s.charAt(i) == s.charAt(i+1)) c++;
        else {
            r += (c > 1 ? c : "") + String.valueOf(s.charAt(i));
            c=1;
        }

    return r;
}


String lineEncoding1(String s) {
    String r="";
	int l, char c;
	Matcher m = Pattern.compile("([a-z])\\1*").matcher(s);
	
    while(m.find()) {
		l = m.group(0).length();
		c = m.group(0).charAt(0);
		r += (c > 1 ? c : "") + String.valueOf(c);
	}

    return r;
}

