int matrixElementsSum(int[][] m) {
    int s = 0;
    
    for(int c=0; c < m[0].length; c++)
        for(int r=0; r < m.length; r++)
            if(m[r][c] > 0) s += m[r][c];
            else break;                            
    
    return s;
}