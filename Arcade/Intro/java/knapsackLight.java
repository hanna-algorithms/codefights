int knapsackLight(int v1, int w1, int v2, int w2, int W) {
    return knapsack01(W, new int[] {w1, w2}, new int[] {v1, v2}, 2);
}

int knapsack01(int W, int w[], int v[], int n) {
    if(W == 0 || n == 0) return 0;	 // base case
        
    if(w[n-1] > W)
        return knapsack01(W, w, v, n-1); // skip item
    else 
		// check max for minus value
        return Math.max(v[n-1] + knapsack01(W - w[n-1], w, v, n-1) // include item - add value, decrease maxWeight
                        , knapsack01(W, w, v, n-1)); // skip item
}

// by Tvc97
int knapsackLight1(int value1, int weight1, int value2, int weight2, int maxW) {
    if(weight1 > maxW && weight2 > maxW) return 0;
    if(weight1 + weight2 <= maxW) return value1 + value2;
    if(weight1 > maxW) return value2;
    if(weight2 > maxW) return value1;
    return Math.max(value1, value2);
}
