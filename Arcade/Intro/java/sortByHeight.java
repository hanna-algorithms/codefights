int[] sortByHeight(int[] a) {
    for(int i=0; i < a.length; i++)
        for(int j = 0 ; j  < i ; j++)
            if(a[i] != -1 && a[j] != -1 && a[i] < a[j])
                a[j] = (a[i] + a[j]) - (a[i] = a[j]); // swap
    return a;
}


int[] sortByHeight(int[] a) {
    int[] b = a;
    
    int j=0;
    for(int i=0; i < a.length; i++)
        if(a[i] != -1) b[j++] = a[i];
    Arrays.sort(b);
    
    int i=0;
    for(int j=0; j < a.length && i < b.length-1; j++)
        if(a[j] != -1) a[j] = b[i++]; 

    return a;
}
