String longestDigitsPrefix(String s) {
	// return inputString.replaceAll("^(\\d*).*","$1");

    String ss="";
    int i=0;

	while(i++ < s.length()) {
		if(!Character.isDigit(s.charAt(i))) break;
		ss += s.charAt(i);
	}

    return ss;
}
