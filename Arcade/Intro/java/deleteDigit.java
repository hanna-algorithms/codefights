int deleteDigit(int n) {
    int m = 0;
    for (int t = 1; t <= n; t *= 10)
        m = Math.max(m, n%t + n/10/t*t);

    return m;
}


int deleteDigit1(int n) {
    String s = String.valueOf(n);
    int t, m = Integer.MIN_VALUE;
    
    for(int i=0; i < s.length(); i++) {
        t = Integer.parseInt(s.substring(0, i) + s.substring(i+1));  
        m = Math.max(m, t);
    }
    
    return m;
}
