String longestWord(String text) {
	//String[] a = text.split("[^a-zA-Z]");
    Matcher m = Pattern.compile("\\w+").matcher(text); 
    String longest = "";
    int max = Integer.MIN_VALUE;
    
    while(m.find()) {
        if(max < m.group(0).length())  {
            longest = m.group(0); 
            max = m.group(0).length();
        }
    }

    return longest;
}


String longestWord2(String text) {
	return Arrays.stream(text.split("\\W+")).max((a, b) -> a.length() - b.length()).get();
}