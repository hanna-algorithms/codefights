boolean isLucky(int n) {
    int c=0;
    
    String s = String.valueOf(n);
    for(int i=0; i < s.length()/2; i++)
        c += (s.charAt(i) - s.charAt(s.length()-1 - i));

    return c == 0;
}
