boolean almostIncreasingSequence(int[] s) {
    int c = 0;

    for(int i=0; i < s.length-1; i++) {
        if (s[i] >= s[i+1])
            c++;
        if (i+2 < s.length && s[i] >= s[i+2])
            c++;
    }
    return c <= 2;
}

boolean almostIncreasingSequence1(int[] s) {
    int m1 = Integer.MIN_VALUE;
    int m2 = Integer.MIN_VALUE;
    int sp = 0;

    for(int i=0; i < s.length; i++) {
        if (s[i] > m1) {
            m2 = m1;
            m1 = s[i];
        } else if (s[i] > m2) {
            m1 = s[i];
            sp++;
        } else
            sp++;
    }
    return sp < 2;
}