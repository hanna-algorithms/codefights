boolean isBeautifulString(String s) {
    int[] a = new int[26];
	for(char c : s.toCharArray())
		a[c-'a']++;
	
	for(int i=0;  i < a.length-1; i++)
		if(a[i] < a[i+1]) return false;

	return true;
}


boolean isBeautifulString1(String s) {
	Integer [] a = new Integer[26];

	for(int i=0; i < s.length(); i++) {
		char cc = s.charAt(i);
		if(cc == 'a') continue;
		char pc = (char) (cc - 1); // previous char in alphabet

		if(a[cc-'a'] == null) a[cc-'a'] = (int) s.chars().filter(c->c==cc).count(); // count occ of the char
		if(a[pc-'a'] == null) a[pc-'a'] = (int) s.chars().filter(c->c==pc).count(); // count occ of prev char
		
		if(a[cc-'a'] > a[pc-'a']) return false; // compare number of occurences
	}
	return true;
}