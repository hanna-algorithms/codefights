boolean stringsRearrangement(String[] inputArray) {
    List<String> arr = new ArrayList<String>();
    for (String s: inputArray) {
        arr.add(s);
    }
    premutations(new ArrayList<String>(), arr);
    boolean ans = false;
    for (ArrayList<String> current: premutations) {
        if(check(current)) ans = true;
    }
    return ans;
}

boolean check(ArrayList<String> ar) {
    for (int i = 1; i < ar.size(); i++) {
        int diffCount = count(ar.get(i-1), ar.get(i));
        if (diffCount != 1) return false;
    }
    return true;
}

int count(String s1, String s2) {
    int count = 0;
    for(int i = 0; i < s1.length(); i++) {
        if (s1.charAt(i) != s2.charAt(i)) count++;
    }
    return count;
}

ArrayList<ArrayList<String>> premutations = new ArrayList<ArrayList<String>>();

void premutations (ArrayList<String> current, List<String> notUsed) {
    if (notUsed.size() == 0) {
        premutations.add(new ArrayList<String>(current));
        return;
    }
    for (String s: notUsed) {
        ArrayList<String> newCurr = new ArrayList<>(current);
        List<String> newNotUsed = new ArrayList<String>(notUsed);
        newCurr.add(s);
        newNotUsed.remove(s);
        premutations(newCurr, newNotUsed);
    }
}

// draft
boolean stringsRearrangement(String[] a) {
    String[] anew = new String[a.length];
    anew[0] = a[0];
    a[0] = null;

    for(int i=0; i < a.length; i++) {
        if(a[i] == null) continue;

        // check within new array
        for(int j=0; j < anew.length; j++) {
            boolean yes = differByOne(a[i], anew[j]);
            if(yes)  {
                anew[j+1] = a[i];
                a[i] = null;
            }
        }

        if(found) {
            // add to new array
            anew[] = a[k];
            // remove from the array
            a[k] = null;
        } else return false;
    }
    return true;
}

boolean differByOne(char[] s1, char[] s2) {
    int c=0;
    for(int i=0; i < s1.length; i++) {
        if (s1[i] != s2[i]) c++;
        if (c > 1) return false; // can't differ more than one
    }
    if (c == 0) return false; // can't be exactly same 
    return true;
}

