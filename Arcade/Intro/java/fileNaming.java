
String[] fileNaming(String[] names) {
    Map<String, Integer> m = new LinkedHashMap();

    for(String n : names)
        m = rename(m, n);
    
    return m.keySet().toArray(new String[m.size()]);
}


Map<String, Integer> rename(Map<String, Integer> m, String n) {
    if(m.containsKey(n)) {
        m.put(n, m.get(n)+1);

        String rn = String.format("%s(%s)", n, m.get(n));
        m = rename(m, m.containsKey(rn) ? n : rn);
    } else m.put(n, 0);

    return m;
}

