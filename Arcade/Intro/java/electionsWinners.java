int electionsWinners(int[] v, int k) {
	int max = IntStream.range(0, v.length).map(i->v[i]).max().getAsInt();
    return k == 0 ? Arrays.stream(v).filter(i->i == max).count() == 1 ? 1 : 0 
		: (int) Arrays.stream(v).filter(i->i+k > max).count();
}

int electionsWinners1(int[] v, int k) {
	Arrays.sort(v);
    int max = v[v.length - 1];

    int c = 0;
    for (int i=v.length-1; i >= 0; i--)
        if (v[i] + k > max) c++;
		else break;
	if (c > 1) return c;

	for (int i=v.length-1; i >= 0; i--)
		if (v[i] == max) c++;
		else if(v[i] < max) break;

    return c == 1 ? 1 : 0;
}


int electionsWinners2(int[] v, int k) {  
    Arrays.sort(v);
    int max = v[v.length - 1];

    // ignore those votes cannot compete for the winner: v[i] + k < max(v)
    HashMap<Integer, Integer> m = new HashMap();
    for(int i=v.length-1; i >= 0; i--) {
        if((v[i] + k) >= max) m.put(v[i], m.containsKey(v[i]) ? m.get(v[i]) + 1 : 1);
        else break;
    }
    
    // pick votes that are forge(!m.containsKey(t + k)) 
    // OR unique m.get(t) == 1
    int c=0;
    for(Integer t : m.keySet())
        if(!m.containsKey(t + k)) c += m.get(t);
        else if(m.get(t) == 1) c += m.get(t);

    return c;
}

