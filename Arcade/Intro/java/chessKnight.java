int chessKnight(String cell) {
    int x = cell.charAt(0) - 'a';
    int y = cell.charAt(1) - '1';

    int[][] s = {{2, 1}, {2, -1}, {-2, 1}, {-2, -1},
                {1, 2}, {1, -2}, {-1, 2}, {-1, -2}};
    
	int c=0;
	for(int i=0; i < 8; i++)
		if((x + s[i][0] < 8 && x + s[i][0] >= 0) 
            && (y + s[i][1] < 8 && y + s[i][1] >= 0)) c++;

	return c;
}


int chessKnight1(String cell) {
    int x = cell.charAt(0) - 'a';
    int y = cell.charAt(1) - '1';
    
    int c=0;
    if(x+2 < 8 && y+1 < 8) c++;
    if(x+2 < 8 && y-1 >= 0) c++;
    if(x-2 >= 0 && y+1 < 8) c++;
    if(x-2 >= 0 && y-1 >= 0) c++;
    if(y+2 < 8 && x+1 < 8) c++;
    if(y+2 < 8 && x-1 >= 0) c++;
    if(y-2 >= 0 && x+1 < 8) c++;
    if(y-2 >= 0 && x-1 >= 0) c++;

    return c;
}
