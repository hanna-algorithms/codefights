boolean isMAC48Address(String s) {
    return Pattern.matches("^([0-9A-F]{2}[-]){5}[0-9A-F]{2}$", s);
}