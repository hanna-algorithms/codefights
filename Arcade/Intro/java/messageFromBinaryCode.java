String messageFromBinaryCode(String code) {
    String s="";
    
    for(int i=0; i < code.length()-7; i+=8)
        s += (char) Integer.parseInt(code.substring(i, i+8), 2);
    
    return s;
}

