int differentSquares(int[][] m) {
    Set<String> set = new HashSet();
    
    for(int i=0; i < m.length-1; i++)
        for(int j=0; j < m[0].length-1; j++)
            set.add(String.format("%d%d%d%d", m[i][j], m[i][j+1], m[i+1][j], m[i+1][j+1]));
    
    return set.size();
}