def centuryFromYear(year):
    return int(year/100) + (1 if year%100 else 0)