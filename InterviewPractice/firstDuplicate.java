int firstDuplicate(int[] a) {
    boolean[] b = new boolean[a.length+1];
    
    for(int i=0; i < a.length; i++)
        if(b[a[i]] == false) b[a[i]] = true;
        else return a[i];
        
    return -1;
}
